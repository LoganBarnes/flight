class Prompt {
  public value: string
  public text: string
  public section: string
  public r22: string
  public cadet: string
  public raven2: string

  constructor(section: string, displayText: string, response?: string) {
    this.value = section + displayText
    this.text = displayText
    this.section = section
    this.cadet = response || ``
    this.raven2 = this.cadet
    this.r22 = this.cadet
  }

  public setR22(response: string): Prompt {
    this.r22 = response
    return this
  }
  public setCadet(response: string): Prompt {
    this.cadet = response
    return this
  }
  public setRaven2(response: string): Prompt {
    this.raven2 = response
    return this
  }

  public update(callback: (self: Prompt) => void): Prompt {
    callback(this)
    return this
  }
}

const prompts: Prompt[] = [
  new Prompt(
    `general`,
    `Definition: Caution`,
    `Equipment damage, injury, or death can result if procedure or instruction is not followed.`
  ),
  new Prompt(`general`, `Definition: Note`, `Provides emphasis or supplementary information.`),
  new Prompt(`general`, `Dimensions (inches)`, ``).update((self: Prompt) => {
    self.raven2 = [
      `Skid Width 86`,
      `Cabin Width 50.5`,
      `Main Rotor Radius 198`,
      `Tail Rotor Diameter 58`,
      `Cabin Height 72`,
      `Nose to Tail 353`,
      `Rotor Tip to Tail 459`,
      `Full Height 129`
    ].join(`\n`)
    self.cadet = [`Full Width 90`, self.raven2].join(`\n`)
  }),
  new Prompt(
    `general`,
    `Main Rotor`,
    [
      `Articulation Free to teeter and cone, rigid inplane`,
      `Number of Blades 2`,
      `Diameter 33 feet`,
      `Blade Chord 10 inches inboard, 10.6 inches outboard`,
      `Blade Twist -6 Degrees`,
      `Tip Speed at 102% RPM 705 FPS`
    ].join(`\n\n`)
  ).setR22(
    [
      `Articulation Free to teeter and cone, rigid inplane`,
      `Number of Blades 2`,
      `Diameter 25 feet 2 inches`,
      `Blade Chord 7.2 inches inboard, 7.7 inches outboard`,
      `Blade Twist -8 degrees`,
      `Tip Speed at 104% RPM 698 feet per second`
    ].join(`\n\n`)
  ),
  new Prompt(
    `general`,
    `Tail Rotor`,
    [
      `Articulation Free to teeter, rigid inplane`,
      `Number of Blades 2`,
      `Diameter 58 inches`,
      `Blade Chord 5.1 inches (constant)`,
      `Blade Twist 0`,
      `Precone Angle 1 Degree`,
      `Tip Speed at 102% RPM 614 FPS`
    ].join(`\n\n`)
  ).setR22(
    [
      `Articulation Free to teeter, rigid inplane`,
      `Number of Blades 2`,
      `Diameter 42 inches`,
      `Blade Chord 4 inches (constant)`,
      `Blade Twist 0 degrees`,
      `Precone Angle 1 degree 11 minutes`,
      `Tip Speed at 104% RPM 622 feet per second`
    ].join(`\n\n`)
  ),
  new Prompt(
    `general`,
    `Drive System`,
    [
      `Engine to Upper Sheave: Four double Vee-belts with 0.778:1 speed reducing ratio`,
      `Upper Sheave to Drive Line: Sprag-type overrunning clutch`,
      `Drive Line to Main Rotor: Spiral-bevel gears with 11:57 speed reducing ratio`,
      `Drive Line to Tail Rotor: Spiral-bevel gears with 31:27 speed increasing ratio`
    ].join(`\n\n`)
  ).update((self: Prompt) => {
    self.r22 = self.r22.replace(`Four`, `Two`)
    self.r22 = self.r22.replace(`0.778:1`, `.8536:1`)
    self.r22 = self.r22.replace(`11:57`, `11:47`)
    self.r22 = self.r22.replace(`31:27`, `3:2`)
  }),
  new Prompt(`general`, `Powerplant`, ``)
    .setCadet(
      [
        `Model: Lycoming O-540-F1B5 (FAA type certificate no. E-295)`,
        ``,
        `Type: Six cylinder, horizontally opposed, direct drive, air cooled, carbureted, normally aspirated`,
        ``,
        `Displacement: 541.5 cubic inches`,
        ``,
        `Normal rating: 260 BHP @ 2800 RPM`,
        ``,
        `R44 Cadet`,
        `Maximum continuous rating: 185 BHP @ 2718 RPM (102% on tachometer)`,
        ``,
        `R44 Cadet`,
        `5 minute takeoff rating: 210 BHP @ 2718 RPM`,
        ``,
        `Cooling: Direct drive squirrel-cage blower`
      ].join(`\n`)
    )
    .setR22(``)
    .setRaven2(
      [
        `Model: Lycoming IO-540-AE1A5`,
        ``,
        `Type: Six cylinder, horizontally opposed, direct drive, air cooled, fuel injected, normally aspirated`,
        ``,
        `Displacement: 541.5 cubic inches`,
        ``,
        `Maximum continuous rating: 205 BHP at 2718 RPM (102% on tachometer)`,
        ``,
        `5 minute takeoff rating: 245 BHP at 2718 RPM`,
        ``,
        `Cooling: Direct drive squirrel-cage blower`
      ].join(`\n`)
    ),
  new Prompt(
    `general`,
    `Critical Altitude`,
    `Altitude at which full throttle produces maximum allowable power (MCP or TOP).`
  ),
  new Prompt(
    `general`,
    `Reference Datum`,
    [
      `A vertical plane from which horizontal distances are measured for balance purposes.`,
      ` The longitudinal reference datum is 100 inches forward of the main rotor shaft centerline for the R44 Cadet.`
    ].join(``)
  ).update((self: Prompt) => {
    self.raven2 = self.cadet.replace(`Cadet`, `II`)
  }),
  new Prompt(
    `general`,
    `Arm`,
    `Horizontal distance from a reference datum to the center of gravity (CG) of an item.`
  ),
  new Prompt(
    `general`,
    `Center of Gravity (CG)`,
    [
      `Location on the fuselage (usually expressed in inches from the reference datum) at which`,
      ` the helicopter would balance. CG is calculated by dividing the total helicopter moment by total helicopter weight.`
    ].join(``)
  ),
  new Prompt(
    `general`,
    `CG Limits`,
    `Extreme CG locations within which the helicopter must be operated at a given weight.`
  ),
  new Prompt(`general`, `Usable Fuel`, `Fuel available for flight planning.`),
  new Prompt(
    `general`,
    `Unusable Fuel`,
    `Fuel remaining in the tank that cannot reliably provide uninterrupted fuel flow in the critical flight attitude.`
  ),
  new Prompt(
    `general`,
    `Standard Empty Weight`,
    `Weight of a standard helicopter including unusable fuel, full operating fluids, and full engine oil.`
  ),
  new Prompt(
    `general`,
    `Basic Empty Weight`,
    `Standard empty weight plus weight of installed optional equipment.`
  ),
  new Prompt(`general`, `Payload`, `Weight of occupants, cargo, and baggage.`),
  new Prompt(
    `general`,
    `Useful load`,
    `Difference between maximum gross weight and basic empty weight.`
  ),
  new Prompt(`limitations`, `Never-Exceed Airspeed (Vne)`)
    .setR22(
      [
        `Up to 3000 feet density altitude: 102 KIAS`,
        `Above 3000 feet density altitude, see placards on page 2-11`
      ].join(`\n`)
    )
    .setCadet([`Power On 120 KIAS`, `Autorotation 100 KIAS`].join(`\n`))
    .setRaven2(
      [
        `2200 lb TOGW and below 130 KIAS`,
        `Over 2200 lb TOGW 120 KIAS`,
        `Autorotation 100 KIAS`
      ].join(`\n`)
    ),
  new Prompt(
    `limitations`,
    `Additional Airspeed Limits`,
    [
      `100 KIAS maximum at power above MCP.`,
      `100 KIAS maximum with any combination of cabin doors removed.`
    ].join(`\n`)
  ).setR22(``),
  new Prompt(
    `limitations`,
    `Rotor Speed Limits`,
    [
      `Power On`,
      `Maximum 102% (408 RPM)`,
      `Minimum* 101% (404 RPM)`,
      ``,
      `Power Off`,
      `Maximum 108% (432 RPM)`,
      `Minimum 90% (360 RPM)`,
      ``,
      `*Transient operation below 101% permitted for emergency procedures training.`
    ].join(`\n`)
  ).setR22(
    [
      `Power On`,
      `Maximum 104% (530 RPM)`,
      `Minimum* 101% (515 RPM)**`,
      ``,
      `Power Off`,
      `Maximum 110% (561 RPM)`,
      `Minimum 97% (459 RPM)`,
      ``,
      `*Transient operation at lower RPM permitted for emergency procedures training.`,
      ``,
      `**97% (495 RPM) permitted on R22s with O-320 engine and tachometer with 97% to 104% green arc installed.`
    ].join(`\n`)
  ),
  new Prompt(`limitations`, `Powerplant Operating Limits`, ``).update((self: Prompt) => {
    self.cadet = [
      `Engine Speed`,
      `Maximum continuous 102% (2718 RPM)`,
      `Maximum transient** 105% (2800 RPM)`,
      ``,
      `Cylinder Head Max Temperature 500F`,
      ``,
      `Oil Maximum Temperature 245F`,
      ``,
      `Oil Pressure`,
      `Minimum during idle 25 psi`,
      `Minimum during flight 55 psi`,
      `Maximum during flight 95 psi`,
      `Maximum during start & warm-up 115 psi`,
      ``,
      `Oil Quantity, minimum for takeoff 7 qt`,
      ``,
      `Manifold Pressure: See placard on page 2-9 for MAP schedule.`,
      ``,
      `**Intentional operation above maximum continuous speed prohibited.`
    ].join(`\n`)
    self.raven2 = self.cadet
  }),
  new Prompt(`limitations`, `Weight Limits`, ``).update((self: Prompt) => {
    self.raven2 = [
      `Maximum gross weight 2500 lb`,
      ``,
      `Minimum gross weight 1600 lb`,
      ``,
      `Maximum per seat including baggage compartment 300 lb`,
      ``,
      `Maximum in any baggage compartment 50 lb`
    ].join(`\n`)
    self.cadet = self.raven2 + `\n\nMaximum on deck above aft baggage compartments 50 lb`
    self.cadet = self.cadet.replace(`2500`, `2200`)
    self.cadet = self.cadet.replace(`1600`, `1550`)
  }),
  new Prompt(
    `limitations`,
    `Prohibited Flight and Maneuvers`,
    [
      `Aerobatic flight prohibited.`,
      `Low-G cyclic pushovers prohibited.`,
      `Flight prohibited with governor selected off, with exceptions for in-flight system malfunction or emergency procedures training.`,
      `Flight in known icing conditions prohibited.`
    ].join(`\n`)
  ).setR22(``),
  new Prompt(
    `limitations`,
    `Aerobatic Flight Caution`,
    [
      `Abrupt control inputs may cause high fatigue stresses`,
      ` and cause catastrophic failure of a critical component.`
    ].join(``)
  ).setR22(``),
  new Prompt(
    `limitations`,
    `Low-G Cyclic Pushover Caution`,
    [
      `A pushover (forward cyclic maneuver) performed`,
      ` from level flight or following a pullup causes a`,
      ` low-G (near weightless) condition which can result`,
      ` in catastrophic loss of lateral control. To eliminate`,
      ` a low-G condition, immediately apply gentle aft`,
      ` cyclic. Should a right roll commence during a`,
      ` low-G condition, apply gentle aft cyclic to reload`,
      ` rotor before applying lateral cyclic to stop roll.`
    ].join(``)
  ).setR22(``),
  new Prompt(
    `limitations`,
    `Altitude Limitations`,
    [
      `Maximum operating density altitude 14,000 feet.`,
      `Maximum operating altitude 9000 feet AGL to allow landing within 5 minutes in case of fire.`
    ].join(`\n`)
  ).setR22(``),
  new Prompt(`limitations`, `Crew Limitations`, ``)
    .setRaven2(
      `Minimum crew is one pilot in the right front seat. A flight instructor may act as pilot in command from the left seat. Solo flight from right seat only.`
    )
    .update((self: Prompt) => {
      self.cadet = [
        self.raven2.replace(`right front`, `right`),
        `Maximum occupancy is two people. Occupants in aft cabin prohibited.`
      ].join(`\n`)
    }),
  new Prompt(`limitations`, `Approved Fuel Grades`, ``).update((self: Prompt) => {
    self.raven2 = [
      `ASTM D910`,
      `100 Green`,
      `100LL Blue`,
      `100VLL Blue`,
      ``,
      `GOST 1012-72 Russian National Standard`,
      `B95/130 Amber`
    ].join(`\n`)

    self.cadet = [
      `ASTM D910`,
      `100 Green`,
      `100LL Blue`,
      `100VLL Blue`,
      ``,
      `ASTM D7547`,
      `UL 91`,
      `UL 94 Clear to Yellow (no dye)`,
      ``,
      `Hjelmco Oil, Inc. Sollentuna, Sweden`,
      `HJELMCO 91/96 UL Clear to Yellow (no dye)`,
      ``,
      `TU 38.5901481-96 Ukrainian National Standard`,
      `91 Yellow`,
      ``,
      `GOST 1012-72 Russian National Standard`,
      `B91/115 Green`,
      `B95/130 Amber`
    ].join(`\n`)
    self.r22 = self.cadet
  }),
  new Prompt(
    `limitations`,
    `Fuel Capacity`,
    [
      `Main tank 30.5 total 29.5 usable`,
      `Auxiliary tank 17.2 total 17.0 usable`,
      `Combined capacity 47.7 total 46.5 usable`
    ].join(`\n`)
  ).setR22(``),
  new Prompt(
    `limitations`,
    `Airspeed Indicator Markings`,
    [
      `Green arc 0 to 110 KIAS`,
      `Yellow arc 110 to 120 KIAS`,
      `Red cross-hatch 100 KIAS`,
      `Red line 120 KIAS`
    ].join(`\n`)
  ).update((self: Prompt) => {
    self.r22 = ``
    self.raven2 = self.cadet
      .replace(`Yellow arc`, `Yellow arc*`)
      .replace(`120`, `130`)
      .replace(`120`, `130`)
    self.raven2 = [
      self.raven2,
      ``,
      `*Earlier airspeed indicators without yellow arc must have the following placard adjacent:`,
      `DO NOT EXCEED 110 KIAS EXCEPT IN SMOOTH AIR`
    ].join(`\n`)
  }),
  new Prompt(
    `limitations`,
    `Rotor Tachometer Markings`,
    [`Lower red line 90%`, `Green arc 90% to 108%`, `Upper red line 108%`].join(`\n`)
  ).setR22(``),
  new Prompt(
    `limitations`,
    `Engine Tachometer Markings`,
    [`Lower red line 101%`, `Green arc 101% to 102%`, `Upper red line 102%`].join(`\n`)
  ).setR22(``),
  new Prompt(
    `limitations`,
    `Oil Pressure Markings`,
    [
      `Lower red line 25 psi`,
      `Lower yellow arc 25 to 55 psi`,
      `Green arc 55 to 95 psi`,
      `Upper yellow arc 95 to 115 psi`,
      `Upper red line 115 psi`
    ].join(`\n`)
  ).setR22(``),
  new Prompt(
    `limitations`,
    `Oil Temperature Markings`,
    [`Green arc 75 to 245F`, `Red line 245F`].join(`\n`)
  ).setR22(``),
  new Prompt(
    `limitations`,
    `Cylinder Head Temperature Markings`,
    [`Green arc 200 to 500F`, `Red line 500F`].join(`\n`)
  ).setR22(``),
  new Prompt(`limitations`, `Manifold Pressure Markings`, ``)
    .setCadet(
      [
        `Green arc 16.0 to 22.9 in. Hg`,
        `Yellow arc 19.5 to 24.9 in. Hg`,
        `Red line 24.9 in. Hg`
      ].join(`\n`)
    )
    .setRaven2(
      [
        `Green arc 15.0 to 23.3 in. Hg`,
        `Yellow arc 19.1 to 26.1 in. Hg`,
        `Red line 26.1 in. Hg`
      ].join(`\n`)
    ),
  new Prompt(`limitations`, `Carburetor Air Temperature Markings`, ``).setCadet(
    `Yellow arc -19C to +3C`
  ),
  new Prompt(
    `emergency`,
    `Land Immediately`,
    `Land on the nearest clear area where a safe normal landing can be performed. Be prepared to enter autorotation during approach, if required.`
  ),
  new Prompt(
    `emergency`,
    `Land As Soon As Practical`,
    `Landing site is at pilot's discretion based on nature of problem and available landing areas. Flight beyond nearest airport is not recommended.`
  ),
  new Prompt(
    `emergency`,
    `Power Failure General`,
    [
      `A power failure may be caused by either an engine or drive system failure`,
      ` and will usually be indicated by the low RPM horn. An engine failure may be`,
      ` indicated by a change in noise level, nose left yaw, an oil pressure light,`,
      ` or decreasing engine RPM. A drive system failure may be indicated by an`,
      ` unusual noise or vibration, nose right or left yaw, or decreasing rotor`,
      ` RPM while engine RPM is increasing.`,
      '\n\n',
      `In case of power failure, immediately lower collective to enter autorotation$`,
      `\n\nCAUTION\n\n`,
      `Aft cyclic is required when collective is lowered at high air speed.`,
      `\n\nCAUTION\n\n`,
      `Do not apply aft cyclic during touchdown or ground slide to prevent possible blade strike to tailcone.`
    ].join(``)
  ).update((self: Prompt) => {
    self.r22 = self.r22.replace(`$`, `.`)
    self.cadet = self.cadet.replace(`$`, ` and reduce airspeed to power-off Vne or below.`)
    self.raven2 = self.cadet
  }),
  new Prompt(
    `emergency`,
    `Power Failure Above 500 Feet AGL`,
    [
      `1. Lower collective immediately to maintain rotor RPM.`,
      ``,
      `2. Establish a steady glide at approximately 70 KIAS. (For maximum glide distance or minimum rate of descent, see page 3-3.)`,
      ``,
      `3. Adjust collective to keep RPM between 97 and 108% or apply full down collective if light weight prevents attaining above 97%.`,
      ``,
      `4. Select landing spot and, if altitude permits, maneuver so landing will be into wind.`,
      ``,
      `5. A restart may be attempted at pilot's discretion if sufficient time is available (See "Air Restart Procedure", page 3-3).`,
      ``,
      `6. If unable to restart, turn unnecessary switches and fuel valve off.`,
      ``,
      `7. At about 40 feet AGL, begin cyclic flare to reduce rate of descent and forward speed.`,
      ``,
      `8. At about 8 feet AGL, apply forward cyclic to level ship and raise collective just before touchdown to cushion landing. Touch down in level attitude with nose straight ahead.`
    ].join(`\n`)
  ).update((self: Prompt) => {
    self.r22 = self.r22.replace(`70`, `65`)
    self.r22 = self.r22.replace(`108`, `110`)
    self.r22 = self.r22.replace(`or minimum rate of descent`, ``)
  }),
  new Prompt(
    `emergency`,
    `Power Failure Between 8 Feet and 500 Feet AGL`,
    [
      `1. Lower collective immediately to maintain rotor RPM.`,
      ``,
      `2. Adjust collective to keep RPM between 97 and 108% or apply full down collective if light weight prevents attaining above 97%.`,
      ``,
      `3. Maintain airspeed until ground is approached, then begin cyclic flare to reduce rate of descent and forward speed.`,
      ``,
      `4. At about 8 feet AGL, apply forward cyclic to level ship and raise collective just before touchdown to cushion landing. Touch down in level attitude with nose straight ahead.`
    ].join(`\n`)
  ).update((self: Prompt) => {
    self.r22 = self.r22.replace(`108`, `110`)
  }),
  new Prompt(
    `emergency`,
    `Power Failure Below 8 Feet AGL`,
    [
      `1. Apply right pedal as required to prevent yawing.`,
      ``,
      `2. Allow helicopter to settle.`,
      ``,
      `3. Raise collective just before touchdown to cushion landing.`
    ].join(`\n`)
  ).update((self: Prompt) => {
    self.r22 = self.r22.replace(`helicopter`, `aircraft`)
    self.raven2 = self.raven2.replace(`helicopter`, `rotorcraft`)
  }),
  new Prompt(
    `emergency`,
    `Power Failure Configurations`,
    [
      `MAXIMUM GLIDE DISTANCE CONFIGURATION`,
      ``,
      `1. Airspeed approximately 90 KIAS.`,
      ``,
      `2. Rotor RPM approximately 90%.`,
      ``,
      `3. Best glide ratio is about 4.7:1 or 1 nautical mile per 1300 feet AGL.`,
      ``,
      `MINIMUM RATE OF DESCENT CONFIGURATION`,
      ``,
      `1. Airspeed approximately 55 KIAS.`,
      ``,
      `2. Rotor RPM approximately 90%.`,
      ``,
      `3. Minimum rate of descent is about 1350 feet per minute. Glide ratio is about 4:1 or 1 nautical mile per 1500 feet AGL.`,
      ``,
      `CAUTION`,
      ``,
      `Increase rotor RPM to 97% minimum or full down collective when autorotating below 500 feet AGL.`
    ].join(`\n`)
  )
    .setR22(
      [
        `MAXIMUM GLIDE DISTANCE CONFIGURATION`,
        ``,
        `1. Airspeed approximately 70 KIAS.`,
        ``,
        `2. Rotor RPM approximately 90%.`,
        ``,
        `3. Best glide ratio is about 4:1 or 1 nautical mile per 1500 feet AGL.`,
        ``,
        `CAUTION`,
        ``,
        `Increase rotor RPM to 97% minimum when autorotating below 500 feet AGL.`
      ].join(`\n`)
    )
    .update((self: Prompt) => {
      self.cadet = self.cadet.replace(`3. `, ``)
      self.cadet = self.cadet.replace(`3. `, ``)
      self.raven2 = self.raven2.replace(`or full down collective`, ``)
    }),
  new Prompt(
    `emergency`,
    `Air Restart Procedure`,
    [
      `CAUTION`,
      ``,
      `Do not attempt restart if engine malfunction is suspected or before safe autorotation is established.`
    ].join(`\n`)
  ).update((self: Prompt) => {
    self.r22 = [
      `1. Mixture - full rich.`,
      ``,
      `2. Primer (if installed) - down and locked.`,
      ``,
      `3. Throttle - closed, the cracked slightly.`,
      ``,
      `4. Actuate starter with left hand.`,
      ``,
      self.r22
    ].join(`\n`)

    self.cadet = [
      self.cadet,
      ``,
      `1. Mixture - full rich.`,
      ``,
      `2. Throttle - closed, then cracked slightly.`,
      ``,
      `3. Actuate starter with left hand.`
    ].join(`\n`)

    self.raven2 = [
      self.raven2 + ` Air restarts not recommended below 2000 feet AGL.`,
      ``,
      `1. Mixture - Off.`,
      ``,
      `2. Throttle - Closed.`,
      ``,
      `3. Starter - Engage.`,
      ``,
      `4. Mixture - Move slowly rich while cranking.`
    ].join(`\n`)
  }),
  new Prompt(
    `emergency`,
    `Emergency Water Landing - Power Off`,
    [
      `1. Follow same procedures as for power failure over land until contacting water. If time permits, unlatch doors prior to water contact.`,
      ``,
      `2. Apply lateral cyclic when aircraft contacts water to stop rotors.`,
      ``,
      `3. Release seat belt and quickly clear aircraft when rotors stop.`
    ].join(`\n`)
  ),
  new Prompt(
    `emergency`,
    `Emergency Water Landing - Power On`,
    [
      `1. Descend to hover above water.`,
      ``,
      `2. Unlatch doors.`,
      ``,
      `3. Passengers exit aircraft.`,
      ``,
      `4. Fly to safe distance from passengers to avoid possible injury by rotors.`,
      ``,
      `5. Switch battery and alternator OFF.`,
      ``,
      `6. Roll throttle off into overtravel spring.`,
      ``,
      `7. Keep aircraft level and apply full collective as aircraft contacts water.`,
      ``,
      `8. Apply lateral cyclic to stop rotors.`,
      ``,
      `9. Release seat belt and quickly clear aircraft when rotors stop.`
    ].join(`\n`)
  ).update((self: Prompt) => {
    self.r22 = self.r22.replace(`Passengers`, `Passenger`)
  }),
  new Prompt(
    `emergency`,
    `Loss of Tail Rotor Thrust in Forward Flight`,
    [
      `Failure is usually indicated by nose right yaw which cannot be corrected by applying left pedal.`,
      ``,
      `1. Immediately enter autorotation.`,
      ``,
      `2. Maintain at least 70 KIAS if practical.`,
      ``,
      `3. Select landing site, roll throttle off into overtravel spring, and perform autorotation landing.`,
      ``,
      `NOTE`,
      ``,
      `When a suitable landing site is not available, the vertical stabilizers may permit limited controlled flight at low power settings and airspeeds above 70 KIAS; however, prior to reducing airspeed, enter full autorotation.`
    ].join(`\n`)
  ),
  new Prompt(
    `emergency`,
    `Loss of Tail Rotor Thrust in Hover`,
    [
      `Failure is usually indicated by nose right yaw which cannot be stopped by applying left pedal.`,
      ``,
      `1. Immediately roll throttle off into overtravel spring and allow aircraft to settle.`,
      ``,
      `2. Raise collective just before touchdown to cushion landing.`
    ].join(`\n`)
  ),
  new Prompt(
    `emergency`,
    `Headset Audio Failure`,
    [
      `If headset audio fails, land as soon as practical.`,
      ``,
      `CAUTION`,
      ``,
      `For aircraft which provide low RPM horn through the audio system, pilot will not hear horn with a failed headset.`
    ].join(`\n`)
  ),
  new Prompt(
    `emergency`,
    `Engine Fire During Start on Ground`,
    [
      `1. Cranking - Continue and attempt to start which would suck flames and excess fuel into engine.`,
      ``,
      `2. If engine starts, run at 60-70% RPM for a short time.`,
      ``,
      `3. Fuel mixture - OFF.`,
      ``,
      `4. Fuel valve - OFF.`,
      ``,
      `5. Battery switch - OFF.`,
      ``,
      `6. If time permits, apply rotor brake to stop rotors.`,
      ``,
      `7. Exit helicopter.`
    ].join(`\n`)
  ).update((self: Prompt) => {
    self.r22 = self.r22.replace(`60-70%`, `50-60%`)
  }),
  new Prompt(
    `emergency`,
    `Engine Fire in Flight`,
    [
      `1. Enter autorotation.`,
      ``,
      `2. Cabin heat - OFF (if installed and time permits).`,
      ``,
      `3. Cabin vent - ON (if time permits).`,
      ``,
      `4. If engine is running, perform normal landing, then pull fuel mixture OFF and shut fuel valve OFF.`,
      `   If engine stops running, shut fuel valve OFF and complete autorotation landing.`,
      ``,
      `5. Battery switch - OFF.`,
      ``,
      `6. If time permits, apply rotor brake to stop rotors.`,
      ``,
      `7. Exit helicopter.`
    ].join(`\n`)
  ).update((self: Prompt) => {
    self.r22 = self.r22.replace(`pull fuel mixture`, `fuel mixture`)
    self.r22 = self.r22.replace(`shut fuel valve`, `fuel valve`)
    self.r22 = self.r22.replace(`shut fuel valve`, `fuel valve`)

    self.raven2 = self.r22

    self.cadet = self.cadet.replace(`installed and `, ``)
    self.raven2 = self.raven2.replace(`installed and `, ``)
  }),
  new Prompt(
    `emergency`,
    `Electrical Fire in Flight`,
    [
      `1. Battery and alternator switches - OFF.`,
      ``,
      `2. Open cabin vents.`,
      ``,
      `3. Land immediately.`,
      ``,
      `4. Fuel mixture OFF and fuel valve OFF.`,
      ``,
      `5. If time permits, apply rotor brake to stop rotors.`,
      ``,
      `6. Exit helicopter.`,
      ``,
      `CAUTION`,
      ``,
      `Low RPM warning system and governor are inoperative with battery and alternator switches both off.`
    ].join(`\n`)
  ).update((self: Prompt) => {
    self.cadet = self.cadet.replace(`Fuel mixture`, `Pull fuel mixture`)
    self.cadet = self.cadet.replace(`fuel valve`, `shut fuel valve`)
  }),
  new Prompt(
    `emergency`,
    `Tachometer Failure`,
    [
      `If rotor or engine tach malfunctions in flight, use remaining tach to monitor RPM. If it is not clear which tach is malfunctioning or if both tachs malfunction, allow governor to control RPM and land as soon as practical.`,
      ``,
      `NOTE`,
      ``,
      `Each tach, the governor, and the low RPM horn are on separate power circuits. A special circuit allows the battery to provide power to the tachs with the battery and alternator switches both off.`
    ].join(`\n`)
  ),
  new Prompt(
    `emergency`,
    `Hydraulic System Failure`,
    [
      `Hydraulic system failure is indicated by heavy or stiff cyclic and collective controls. Loss of hydraulic fluid may cause intermittent and/or vibrating feedback in the controls. Controls will be normal except for the increase in stick forces.`,
      ``,
      `1. HYD switch - verify ON.`,
      ``,
      `2. If hydraulics not restored, HYD Switch - OFF.`,
      ``,
      `3. Adjust airspeed and flight condition as desired for comfortable control.`,
      ``,
      `4. Land as soon as practical.`
    ].join(`\n`)
  ).setR22(``),
  new Prompt(
    `emergency`,
    `Governor Failure`,
    [
      `If engine RPM governor malfunctions, grip throttle firmly to`,
      ` override governor, then switch governor off. Complete flight`,
      ` using manual throttle control.`
    ].join(``)
  ),
  new Prompt(
    `emergency`,
    `Warning/Caution Lights - NOTE`,
    `If a light causes excessive glare at night, bulb may be unscrewed or circuit breaker pulled to eliminate glare during landing.`
  ),
  new Prompt(
    `emergency`,
    `Warning/Caution Lights - OIL`,
    [
      `Indicates loss of engine power or oil pressure.`,
      ` Check engine tach for power loss. Check oil`,
      ` pressure gage and, if pressure loss is confirmed,`,
      ` land immediately. Continued operation without oil`,
      ` pressure will cause serious engine damage and`,
      ` engine failure may occur.`
    ].join(``)
  ),
  new Prompt(
    `emergency`,
    `Warning/Caution Lights - ENG FIRE`,
    `Indicates possible fire in engine compartment. See procedures on page 3-6.`
  ),
  new Prompt(
    `emergency`,
    `Warning/Caution Lights - MR TEMP/MR CHIP/TR CHIP`,
    [
      `MR TEMP`,
      ``,
      `Indicates excessive temperature of main rotor gearbox. See note below.`,
      ``,
      `MR CHIP`,
      ``,
      `Indicates metallic particles in main rotor gearbox. See note below.`,
      ``,
      `TR CHIP`,
      ``,
      `Indicates metallic particles in tail rotor gearbox. See note below.`,
      ``,
      `NOTE`,
      ``,
      `If light is accompanied by any indication of a problem such as noise, vibration, or temperature rise, land immediately. If there is no other indication of a problem, land as soon as practical.`,
      ``,
      `Break-in fuzz will occasionally activate chip lights. If no metal chips or slivers are found on detector plug, clean and reinstall (tail rotor gearbox must be refilled with new oil). Hover for at least 30 minutes. If chip light comes on again, replace gearbox before further flight.`
    ].join(`\n`)
  ).update((self: Prompt) => {
    self.cadet = self.cadet.replace(`(tail rotor`, `(tail`)
  }),
  new Prompt(
    `emergency`,
    `Warning/Caution Lights - LOW FUEL`,
    [
      `$`,
      ``,
      `CAUTION`,
      ``,
      `Do not use low fuel caution light as a working indication of fuel quantity.`
    ].join(`\n`)
  ).update((self: Prompt) => {
    const r22 = [
      `Indicates approximately one gallon of usable fuel remaining for all-aluminum`,
      ` fuel tanks or 1.5 gallons for bladders-style tanks. The engine will run out of`,
      ` fuel after approximately five minutes at cruise power for aircraft with all-.`,
      `aluminum tanks or ten minutes bladder-style tanks.`
    ].join(``)
    const r44 = `Indicates approximately three gallons of usable fuel remaining. The engine will run out of fuel after ten minutes at cruise power.`

    self.r22 = self.r22.replace(`$`, r22)
    self.cadet = self.cadet.replace(`$`, r44)
    self.raven2 = self.raven2.replace(`$`, r44)
    self.raven2 = self.raven2.replace(`low fuel caution light`, `LOW FUEL`)
  }),
  new Prompt(`emergency`, `Warning/Caution Lights - AUX FUEL PUMP`, ``).setRaven2(
    `Indicates low auxiliary fuel pump pressure. If no other indication of a problem, land as soon as practical. If light is accompanied by erratic engine operation, land immediately.`
  ),
  new Prompt(`emergency`, `Warning/Caution Lights - FUEL FILTER`, ``).setRaven2(
    `Indicates fuel strainer contamination. If no other indication of a problem, land as soon as practical. If light is accompanied by aux fuel pump warning light or erratic engine operation, land immediately.`
  ),
  new Prompt(
    `emergency`,
    `Warning/Caution Lights - CLUTCH`,
    [
      `Indicates clutch actuator circuit is on, either engaging or disengaging clutch.`,
      ` When switch is in the ENGAGE position, light stays on until belts are properly tensioned.`,
      ` Never take off before light goes out.`,
      `\n\nNOTE\n\n`,
      `Clutch light may come on momentarily during run-ups or during flight to retension belts as they warm-up and stretch slightly.`,
      ` This is normal. If however, the light flickers or comes on in flight and does not go out within 10 seconds,`,
      ` pull CLUTCH circuit breaker and land as soon as practical.`,
      ` Reduce power and land immediately if there are other indications of drive system failure (be prepared to enter autorotation).`,
      ` Have drive system inspected for a possible malfunction.`
    ].join(``)
  ).update((self: Prompt) => {
    self.r22 = self.r22.replace(`run-ups`, `run-up`)
    self.cadet = self.cadet.replace(`run-ups`, `run-up`)
  }),
  new Prompt(
    `emergency`,
    `Warning/Caution Lights - ALT`,
    [
      `Indicates low voltage and possible alternator failure.`,
      ` Turn off nonessential electrical equipment and switch ALT off and back on after one second`,
      ` to reset alternator control unit. If light stays on, land as soon as practical.`,
      ` Continued flight without functioning alternator can result in loss of power to tachometers,`,
      ` producing a hazardous flight condition.`
    ].join(``)
  ).update((self: Prompt) => {
    self.cadet = self.cadet.replace(`off and back`, `off then back`)
    self.raven2 = self.raven2.replace(`off and back`, `off then back`)
  }),
  new Prompt(
    `emergency`,
    `Warning/Caution Lights - STARTER ON`,
    [
      `Indicates starter motor is engaged.`,
      ` If light does not go out when ignition switch is released from start position,`,
      ` immediately pull mixture off and turn battery switch off.`,
      ` Have starter motor serviced.`
    ].join(``)
  ).update((self: Prompt) => {
    self.raven2 = self.raven2.replace(
      `ignition switch is released from start position`,
      `starter button is released`
    )
  }),
  new Prompt(
    `emergency`,
    `Warning/Caution Lights - LOW RPM`,
    [
      `Indicates rotor speed below 97% RPM.`,
      ` To restore RPM, immediately lower collective, roll throttle on and, in forward flight,`,
      ` apply aft cyclic. Light is disabled when collective is full down.`
    ].join(``)
  ),
  new Prompt(
    `emergency`,
    `Warning/Caution Lights - GOV OFF`,
    `Indicates engine RPM governor is switched off.`
  ),
  new Prompt(
    `emergency`,
    `Warning/Caution Lights - CARBON MONOXIDE`,
    [
      `Indicates elevated levels of carbon monoxide (CO) in cabin.`,
      ` Shut off heater and open nose and door vents.`,
      ` If hovering, land or transition to forward flight.`,
      ` If symptoms of CO poisoning (headache, drowsiness, dizziness) accompany light, land immediately.`
    ].join(``)
  ),
  new Prompt(
    `emergency`,
    `Warning/Caution Lights - BRAKE`,
    [
      `Indicates rotor brake is engaged.`,
      ` Release immediately in flight or before starting engine.`
    ].join(``)
  ),
  new Prompt(
    `emergency`,
    `Warning/Caution Lights - FULL THROTTLE`,
    [
      `(if installed)\n`,
      `Indicates engine near full throttle.`,
      ` The governor will be ineffective because it cannot increase throttle to maintain RPM.`,
      ` Lower collective as required to extinguish light.`
    ].join(``)
  ),
  new Prompt(
    `emergency`,
    `Warning/Caution Lights - HYD`,
    `(if installed)\nIndicates hydraulic system is switched off.`
  ).setR22(``),
  new Prompt(
    `emergency`,
    `Audio Alerts - LOW RPM HORN`,
    [
      `Horn is provided by one or two speakers in the side of the instrument console`,
      ` on earlier aircraft or through the audio system on later aircraft.`,
      ` The horn activates simultaneously with the LOW RPM caution light`,
      ` and indicates rotor speed below 97% RPM.`,
      ` To restore RPM, lower collective, roll throttle on and, in forward flight, apply aft cyclic.`,
      ` Horn and light are disabled when collective is full down.`
    ].join(``)
  ).update((self: Prompt) => {
    self.r22 = self.r22.replace(`Horn and light`, `Horn and Light`)
    self.r22 = self.r22.replace(`one or two speakers`, `a speaker`)
    self.cadet = self.cadet.replace(`one or two `, ``)
  }),
  new Prompt(
    `emergency`,
    `Audio Alerts - HIGH RPM WARBLE`,
    [
      `On later aircraft, a warble (high/low tone) in the audio system`,
      ` indicates rotor speed is approaching the 108% RPM limit.`,
      ` Raise collective as required to control RPM.`
    ].join(``)
  ).update((self: Prompt) => {
    self.r22 = self.r22.replace(`the 108%`, `110%`)
  }),
  new Prompt(`emergency`, `RIGHT ROLL IN LOW "G" CONDITION`).setR22(
    [
      `Gradually apply aft cyclic to restore positive “G”`,
      ` forces and main rotor thrust. Do not apply lateral`,
      ` lateral cyclic until positive “G” forces have been established.`
    ].join(``)
  ),
  new Prompt(
    `emergency`,
    `UNCOMMANDED PITCH, ROLL. OR YAW RESULTING FROM FLIGHT IN TURBULENCE`
  ).setR22(
    [
      `Gradually apply controls to maintain rotor RPM,`,
      ` positive “G” forces, and to eliminate sideslip.`,
      ` Minimize cyclic control inputs in turbulence;`,
      ` do not overcontrol.`
    ].join(``)
  ),
  new Prompt(
    `emergency`,
    `INADVERTENT ENCOUNTER WITH MODERATE, SEVERE, OR EXTREME TURBULENCE`
  ).setR22(
    [
      `If the area of turbulence is isolated, depart the area;`,
      ` otherwise, land the helicopter as soon as practical.`
    ].join(``)
  ),
  new Prompt(`normal`, `Takeoff and Climb Airspeed`),
  new Prompt(`normal`, `Maximum Rate of Climb Airspeed (Vy)`),
  new Prompt(`normal`, `Maximum Range Airspeed`),
  new Prompt(`normal`, `Maximum Cruise Airspeed`),
  new Prompt(`normal`, `Significant Turbulence Airspeed`),
  new Prompt(`normal`, `Landing Approach Airspeed`),
  new Prompt(`normal`, `Autorotation Airspeed`),
  new Prompt(`normal`, `Airspeed*`)
]

function getPrompts(model: string, section: string): Prompt[] {
  return prompts.filter(
    (i: Prompt) => (section === `all` || i.section === section) && i[model as keyof Prompt]
  )
}

export function getQuestions(model: string, section: string): { text: string; value: string }[] {
  return getPrompts(model, section)
}

function getAnswers(model: string, section: string): Map<string, string> {
  return new Map(
    getPrompts(model, section).map((i: Prompt) => [i.value, i[model as keyof Prompt] as string])
  )
}

export function getAnswer(model: string, section: string, question: string): string {
  return getAnswers(model, section).get(question) || ``
}
