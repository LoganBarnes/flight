class Prompt {
  public value: string
  public text: string
  public content: string

  constructor(displayText: string, response: string) {
    this.value = displayText
    this.text = displayText
    this.content = response
  }
}

const prompts: Prompt[] = [
  new Prompt(
    `ADM (short)`,
    [
      `Aeronautical Decision Making`,
      `\n`,
      `A systematic approach to risk assessment and stress management.`
    ].join(``)
  ),
  new Prompt(
    `ADM (long)`,
    [
      `Aeronautical Decision Making`,
      `\n`,
      `A systematic approach to the mental process used by pilots to consistently`,
      ` determine the best course of action in response to a given set of circumstances`
    ].join(``)
  ),
  new Prompt(
    `SRM (short)`,
    [
      `Single Pilot Resource Management`,
      `\n`,
      `The ability for a pilot to manage all resources effectively to `,
      `ensure the outcome of the flight is successful.`
    ].join(``)
  ),
  new Prompt(
    `SRM (long)`,
    [
      `Single Pilot Resource Management`,
      `\n`,
      `The art and science of managing all the resources (both on-board `,
      `the aircraft and from outside sources) available to a single pilot `,
      `(prior to and during flight) to ensure the successful outcome of the flight.`
    ].join(``)
  ),
  new Prompt(
    `CRM (short)`,
    [
      `Crew Resource Management`,
      `\n`,
      `The application of team management concepts in the flight deck environment.`
    ].join(``)
  ),
  new Prompt(
    `CRM (long)`,
    [
      `Crew Resource Management`,
      `\n`,
      `The effective use of all available resources: human resources, `,
      `hardware, and information supporting ADM to facilitate crew `,
      `cooperation and improve decision-making.`
    ].join(``)
  ),
  new Prompt(
    `Risk Management`,
    [
      `The part of the decision-making process which relies on `,
      `situational awareness, problem recognition, and good `,
      `judgment to reduce risks associated with each flight.`
    ].join(``)
  ),
  new Prompt(
    `Situational Awareness (short)`,
    [
      `Pilot knowledge of where the aircraft is in regard `,
      `to location, air traffic control, weather, regulations, `,
      `aircraft status, and other factors that may affect flight.`
    ].join(``)
  ),
  new Prompt(
    `Situational Awareness (long)`,
    [
      `The accurate perception and understanding of all `,
      `the factors and conditions within the five fundamental `,
      `risk elements (flight, pilot, aircraft, environment, and `,
      `type of operation that comprise any given aviation situation) `,
      `that affect safety before, during, and after the flight.`
    ].join(``)
  ),
  new Prompt(
    `Workload Management (short)`,
    [`Performing high workload tasks during low workload portions of flight.`].join(``)
  ),
  new Prompt(
    `Workload Management (long)`,
    [
      `Ensures essential operations are accomplished by planning, `,
      `prioritizing, and sequencing tasks to avoid work overload.`
    ].join(``)
  ),
  new Prompt(
    `Spatial Disorientation (short)`,
    [
      `Lack of orientation with regard to the position, `,
      `attitude, or movement of the airplane in space.`
    ].join(``)
  ),
  new Prompt(
    `Spatial Disorientation (long)`,
    [
      `The state of confusion due to misleading information `,
      `being sent to the brain from various sensory organs, `,
      `resulting in a lack of awareness of the aircraft position `,
      `in relation to a specific reference point.`
    ].join(``)
  ),
  new Prompt(
    `Motion Sickness`,
    [
      `Nausea caused by motion. The brain receives conflicting `,
      `messages about the state of the body.`
    ].join(``)
  )
]

function getPrompts(): Prompt[] {
  return prompts
}

export function getQuestions(): { text: string; value: string }[] {
  return getPrompts()
}

function getAnswers(): Map<string, string> {
  return new Map(getPrompts().map((i: Prompt) => [i.value, i.content]))
}

export function getAnswer(question: string): string {
  return getAnswers().get(question) || ``
}
