import {
  UpdateLoop,
  WglBuffer,
  WglFramebuffer,
  WglProgram,
  WglRenderbuffer,
  WglShader,
  WglTexture,
  WglVertexArray
} from '../graphics'
import type { UpdateLoopStatus } from '../graphics'
import screenspaceVert from '../assets/shaders/screenspace.vert'
import initialFlowFrag from '../assets/shaders/initialFlow.frag'
import { vec2 } from 'gl-matrix'

// const rho0 = 100 // average density
// const tau = 0.6 // collision timescale

class Flow {
  public texture: WglTexture
  public renderbuffer: WglRenderbuffer
  public framebuffer: WglFramebuffer

  constructor(gl: WebGLRenderingContext) {
    this.texture = new WglTexture(gl)
    this.renderbuffer = new WglRenderbuffer(gl)
    this.framebuffer = new WglFramebuffer(gl)
  }

  public rebind(gl: WebGLRenderingContext): Flow {
    this.framebuffer.bind((boundFramebuffer) => {
      this.renderbuffer.bind((boundRenderbuffer) => {
        boundFramebuffer.framebufferRenderbuffer(gl.DEPTH_ATTACHMENT, boundRenderbuffer)
      })

      this.texture.bind(gl.TEXTURE_2D, (boundTexture) => {
        boundFramebuffer.framebufferTexture2D(gl.COLOR_ATTACHMENT0, boundTexture)
      })

      boundFramebuffer.checkFramebufferStatus()
    })

    return this
  }
}

class ScreenspacePipeline {
  private gl: WebGLRenderingContext
  private program: WglProgram
  private vao: WglVertexArray
  private vbo: WglBuffer

  constructor(gl: WebGLRenderingContext, frag: string, vbo: WglBuffer) {
    this.gl = gl
    this.program = new WglProgram(gl)
    this.program.attachAndLink(
      new WglShader(gl, gl.VERTEX_SHADER).loadAndCompile(screenspaceVert),
      new WglShader(gl, gl.FRAGMENT_SHADER).loadAndCompile(frag)
    )
    this.vao = new WglVertexArray(gl, 0, [
      {
        location: this.program.getAttribLocation(`clipPosition`),
        size: 2,
        type: gl.FLOAT,
        offset: 0
      }
    ])
    this.vbo = vbo
  }

  public bind(
    usage: (boundPipeline: InstanceType<typeof ScreenspacePipeline.Bound>) => void
  ): void {
    const gl = this.gl

    this.vbo.bind(gl.ARRAY_BUFFER, (boundVbo) => {
      this.vao.enableAttribs(boundVbo)

      this.program.use((boundProgram) => {
        usage(new ScreenspacePipeline.Bound(this, boundProgram, boundVbo))
      })
    })
  }

  static Bound = class {
    constructor(
      public self: ScreenspacePipeline,
      public boundProgram: InstanceType<typeof WglProgram.Bound>,
      public boundVbo: InstanceType<typeof WglBuffer.Bound>
    ) {
      this.self = self
      this.boundProgram = boundProgram
      this.boundVbo = boundVbo
    }

    public draw(): InstanceType<typeof ScreenspacePipeline.Bound> {
      const gl = this.self.gl

      this.self.vao.drawArrays(this.boundProgram, this.boundVbo, gl.TRIANGLE_STRIP, 0, 4)

      return this
    }
  }
}

export class LessonTest {
  private gl: WebGLRenderingContext
  private updateLoop: UpdateLoop

  private vbo: WglBuffer
  private flowPipeline: ScreenspacePipeline
  private displayPipeline: ScreenspacePipeline

  private size: vec2

  private flow: Flow[]
  private frame: number

  constructor(gl: WebGLRenderingContext) {
    this.gl = gl
    this.updateLoop = new UpdateLoop(this.fixedStepUpdate.bind(this), this.frameUpdate.bind(this))

    setGlDefaults(gl)

    const vboData = new Float32Array([-1, -1, +1, -1, -1, +1, +1, +1])

    this.vbo = new WglBuffer(gl)
    this.vbo.bind(gl.ARRAY_BUFFER, (boundBuffer) => {
      boundBuffer.bufferData(vboData)
    })

    this.flowPipeline = new ScreenspacePipeline(gl, initialFlowFrag, this.vbo)
    this.displayPipeline = new ScreenspacePipeline(gl, initialFlowFrag, this.vbo)

    this.size = vec2.fromValues(1, 1)
    this.flow = [new Flow(gl), new Flow(gl)]
    this.frame = 0
  }

  public run(): void {
    this.updateLoop.run()
  }

  public stop(): void {
    this.updateLoop.stop()
  }

  public resize(width: number, height: number) {
    const gl = this.gl

    this.size = vec2.fromValues(width, height)

    this.flow.forEach((flow) => {
      flow.texture.bind(gl.TEXTURE_2D, (boundTexture) => {
        boundTexture.setFilterAndWrapParameters(gl.NEAREST, gl.CLAMP_TO_EDGE)
        boundTexture.texImage2D(this.size, null, gl.RGBA, gl.RGBA, gl.FLOAT)
      })

      flow.renderbuffer.bind((renderbuffer) => {
        renderbuffer.renderbufferStorage(gl.DEPTH_COMPONENT16, this.size)
      })

      flow.rebind(gl)
    })

    this.frame = 0
    const flow = this.flow[this.frame]

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    flow.framebuffer.bind((_) => {
      this.gl.viewport(0, 0, this.size[0], this.size[1])
      this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT)
    })
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  private fixedStepUpdate(_status: UpdateLoopStatus) {
    const gl = this.gl

    const flowCurr = this.flow[this.frame]
    const flowPrev = this.flow[(this.frame + 1) % 2]

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    flowCurr.framebuffer.bind((_) => {
      gl.viewport(0, 0, this.size[0], this.size[1])
      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

      this.displayPipeline.bind((boundPipeline) => {
        flowPrev.texture.activateAndBind(gl.TEXTURE_2D, 0, (boundTexture) => {
          boundPipeline.boundProgram.setTextureUniform(`flowMap`, boundTexture, 0)

          boundPipeline.draw()
        })
      })
    })

    this.frame = (this.frame + 1) % 2
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  private frameUpdate(_status: UpdateLoopStatus) {
    const gl = this.gl
    const flow = this.flow[this.frame]

    gl.viewport(0, 0, this.size[0], this.size[1])
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

    this.displayPipeline.bind((boundPipeline) => {
      flow.texture.activateAndBind(gl.TEXTURE_2D, 0, (boundTexture) => {
        boundPipeline.boundProgram.setTextureUniform(`flowMap`, boundTexture, 0)

        boundPipeline.draw()
      })
    })
  }
}

function setGlDefaults(gl: WebGLRenderingContext) {
  gl.clearColor(0.0, 0.0, 0.0, 1.0)

  // Enable depth testing, so that objects are occluded based
  // on depth instead of drawing order.
  gl.enable(gl.DEPTH_TEST)

  // Move the polygons back a bit so lines are still drawn
  // even though they are coplanar with the polygons they
  // came from, which will be drawn before them.
  gl.enable(gl.POLYGON_OFFSET_FILL)
  gl.polygonOffset(-1, -1)

  // Enable back-face culling, meaning only the front side
  // of every face is rendered.
  gl.enable(gl.CULL_FACE)
  gl.cullFace(gl.BACK)

  // Specify that the front face is represented by vertices in
  // a counterclockwise order (this is the default).
  gl.frontFace(gl.CCW)
}
