import {
  WglBuffer,
  WglFramebuffer,
  WglProgram,
  WglRenderbuffer,
  WglShader,
  WglTexture,
  WglVertexArray
} from '../graphics'
import type { UpdateLoopStatus } from '../graphics'
import moleculeComputeVert from '../assets/shaders/moleculeCompute.vert'
import moleculeComputeFrag from '../assets/shaders/moleculeCompute.frag'
import moleculeSpatialVert from '../assets/shaders/moleculeSpatial.vert'
import moleculeSpatialFrag from '../assets/shaders/moleculeSpatial.frag'
import { vec2 } from 'gl-matrix'

export interface MoleculeOptions {
  groundTemperature: number
  gravity: number
  pointRadius: number
  startVelocityScale: number
}

class MoleculeData {
  public texture: WglTexture
  public renderbuffer: WglRenderbuffer
  public framebuffer: WglFramebuffer

  constructor(gl: WebGLRenderingContext) {
    this.texture = new WglTexture(gl)
    this.renderbuffer = new WglRenderbuffer(gl)
    this.framebuffer = new WglFramebuffer(gl)
  }

  public resize(gl: WebGLRenderingContext, size: vec2): void {
    this.texture.bind(gl.TEXTURE_2D, (boundTexture) => {
      boundTexture.setFilterAndWrapParameters(gl.NEAREST, gl.CLAMP_TO_EDGE)
      boundTexture.texImage2D(size, null, gl.RGBA, gl.RGBA, gl.FLOAT)
    })
    this.renderbuffer.bind((renderbuffer) => {
      renderbuffer.renderbufferStorage(gl.DEPTH_COMPONENT16, size)
    })
    this.rebind(gl)
  }

  public rebind(gl: WebGLRenderingContext): MoleculeData {
    this.framebuffer.bind((boundFramebuffer) => {
      this.renderbuffer.bind((boundRenderbuffer) => {
        boundFramebuffer.framebufferRenderbuffer(gl.DEPTH_ATTACHMENT, boundRenderbuffer)
      })

      this.texture.bind(gl.TEXTURE_2D, (boundTexture) => {
        boundFramebuffer.framebufferTexture2D(gl.COLOR_ATTACHMENT0, boundTexture)
      })

      boundFramebuffer.checkFramebufferStatus()
    })

    return this
  }
}

export class MoleculeCompute {
  private gl: WebGLRenderingContext
  private moleculeOptions: MoleculeOptions

  private computeProgram: WglProgram
  private computeVbo: WglBuffer
  private computeVao: WglVertexArray

  private moleculeTextureSize = vec2.fromValues(64, 64)
  private moleculeCount: number

  private prevData: MoleculeData
  private currData: MoleculeData
  private initialized = false

  private worldSize: vec2

  private spatialProgram: WglProgram
  private spatialVbo: WglBuffer
  private spatialVao: WglVertexArray
  private spatialData: MoleculeData

  constructor(gl: WebGLRenderingContext, moleculeOptions: MoleculeOptions) {
    this.gl = gl
    this.moleculeOptions = moleculeOptions

    this.computeVbo = new WglBuffer(gl)
    this.computeVbo.bind(gl.ARRAY_BUFFER, (boundBuffer) => {
      boundBuffer.bufferData(new Float32Array([-1, -1, +1, -1, -1, +1, +1, +1]))
    })

    const moleculeComputeFragMod = moleculeComputeFrag.replace(
      `replace_with_particle_tex_size`,
      this.moleculeTextureSize.toString()
    )
    this.computeProgram = new WglProgram(gl)
    this.computeProgram.attachAndLink(
      new WglShader(gl, gl.VERTEX_SHADER).loadAndCompile(moleculeComputeVert),
      new WglShader(gl, gl.FRAGMENT_SHADER).loadAndCompile(moleculeComputeFragMod)
    )

    this.computeVao = new WglVertexArray(gl, 0, [
      {
        location: this.computeProgram.getAttribLocation(`cornerClipPos`),
        size: 2,
        type: gl.FLOAT,
        offset: 0
      }
    ])

    this.moleculeTextureSize = vec2.fromValues(128, 128)
    this.moleculeCount = this.moleculeTextureSize[0] * this.moleculeTextureSize[1]

    this.prevData = new MoleculeData(gl)
    this.prevData.resize(gl, this.moleculeTextureSize)
    this.currData = new MoleculeData(gl)
    this.currData.resize(gl, this.moleculeTextureSize)

    this.worldSize = vec2.fromValues(1, 1)

    this.spatialProgram = new WglProgram(gl)
    this.spatialProgram.attachAndLink(
      new WglShader(gl, gl.VERTEX_SHADER).loadAndCompile(moleculeSpatialVert),
      new WglShader(gl, gl.FRAGMENT_SHADER).loadAndCompile(moleculeSpatialFrag)
    )

    {
      const moleculeIndexData = new Float32Array(2 * this.count)
      for (let yi = 0; yi < this.textureSize[1]; ++yi) {
        for (let xi = 0; xi < this.textureSize[0]; ++xi) {
          const i = this.textureSize[0] * yi + xi
          moleculeIndexData[i * 2 + 0] = xi + 0.5
          moleculeIndexData[i * 2 + 1] = yi + 0.5
        }
      }

      this.spatialVbo = new WglBuffer(gl)
      this.spatialVbo.bind(gl.ARRAY_BUFFER, (boundBuffer) => {
        boundBuffer.bufferData(moleculeIndexData)
      })
    }

    this.spatialVao = new WglVertexArray(gl, 0, [
      {
        location: this.spatialProgram.getAttribLocation(`moleculeIndex`),
        size: 2,
        type: gl.FLOAT,
        offset: 0
      }
    ])
    this.spatialData = new MoleculeData(gl)
    this.spatialData.resize(gl, this.worldSize)
  }

  public resetWorld(worldSize: vec2, forceInitialize: boolean = false) {
    const gl = this.gl

    this.worldSize = worldSize

    if (!this.initialized || forceInitialize) {
      const radius = this.options.pointRadius
      const diameter = 2 * radius

      // TODO: Distribute by density
      const moleculeData = new Float32Array(4 * this.moleculeCount)
      for (let i = 0; i < this.moleculeCount; ++i) {
        moleculeData[4 * i + 0] = Math.random() * (this.worldSize[0] - diameter) + radius
        moleculeData[4 * i + 1] = Math.random() * (this.worldSize[1] - diameter) + radius
        moleculeData[4 * i + 2] = (Math.random() * 2.0 - 1.0) * this.options.startVelocityScale
        moleculeData[4 * i + 3] = (Math.random() * 2.0 - 1.0) * this.options.startVelocityScale
      }

      this.currMolecules.bind(gl.TEXTURE_2D, (boundTexture) => {
        boundTexture.texImage2D(this.moleculeTextureSize, moleculeData, gl.RGBA, gl.RGBA, gl.FLOAT)
      })

      this.initialized = true
    }

    this.spatialData.resize(gl, this.worldSize)
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public update(status: UpdateLoopStatus) {
    const gl = this.gl

    this.swapData()

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    this.currData.framebuffer.bind((_) => {
      gl.viewport(0, 0, this.moleculeTextureSize[0], this.moleculeTextureSize[1])
      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

      this.computeVbo.bind(gl.ARRAY_BUFFER, (boundVbo) => {
        this.computeVao.enableAttribs(boundVbo)

        this.computeProgram.use((boundProgram) => {
          this.prevMolecules.activateAndBind(gl.TEXTURE_2D, 0, (boundPrevMoleculeTexture) => {
            boundProgram
              .setTextureUniform(`prevMolecules`, boundPrevMoleculeTexture, 0)
              .setFloatUniform(`textureSize`, this.moleculeTextureSize)
              .setFloatUniform(`worldSize`, this.worldSize)
              .setFloatUniform(`timeStepSecs`, [status.updateTimeStepSecs])
              .setFloatUniform(`groundTemperature`, [this.moleculeOptions.groundTemperature])
              .setFloatUniform(`gravity`, [this.moleculeOptions.gravity])
              .setFloatUniform(`pointRadius`, [this.moleculeOptions.pointRadius])

            this.computeVao.drawArrays(boundProgram, boundVbo, gl.TRIANGLE_STRIP, 0, 4)
          })
        })
      })
    })
  }

  private renderSpatial(): void {
    const gl = this.gl

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    this.spatialData.framebuffer.bind((_) => {
      gl.viewport(0, 0, this.worldSize[0], this.worldSize[1])
      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

      this.spatialVbo.bind(gl.ARRAY_BUFFER, (boundVbo) => {
        this.spatialVao.enableAttribs(boundVbo)

        this.spatialProgram.use((boundProgram) => {
          this.currMolecules.activateAndBind(gl.TEXTURE_2D, 0, (boundCurrMoleculeTexture) => {
            boundProgram
              .setTextureUniform(`molecules`, boundCurrMoleculeTexture, 0)
              .setFloatUniform(`textureSize`, this.textureSize)
              .setFloatUniform(`worldSize`, this.worldSize)

            this.spatialVao.drawArrays(boundProgram, boundVbo, gl.POINTS, 0, this.count)
          })
        })
      })
    })
  }

  set options(moleculeOptions: MoleculeOptions) {
    this.moleculeOptions = moleculeOptions
  }

  get count(): number {
    return this.moleculeCount
  }

  get textureSize(): vec2 {
    return this.moleculeTextureSize
  }

  get options(): MoleculeOptions {
    return this.moleculeOptions
  }

  get prevMolecules(): WglTexture {
    return this.prevData.texture
  }

  get currMolecules(): WglTexture {
    return this.currData.texture
  }

  get indexVbo(): WglBuffer {
    return this.spatialVbo
  }

  private swapData(): void {
    const tmp = this.prevData
    this.prevData = this.currData
    this.currData = tmp
  }
}
