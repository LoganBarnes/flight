import { UpdateLoop, WglProgram, WglShader, WglVertexArray } from '../graphics'
import type { UpdateLoopStatus } from '../graphics'
import { MoleculeCompute } from './MoleculeCompute'
import type { MoleculeOptions } from '../exercises/MoleculeCompute'
import moleculeDisplayVert from '../assets/shaders/moleculeDisplay.vert'
import moleculeDisplayFrag from '../assets/shaders/moleculeDisplay.frag'
import { vec2 } from 'gl-matrix'

export class MoleculeLesson {
  private gl: WebGLRenderingContext
  private updateLoop: UpdateLoop

  private molecules: MoleculeCompute

  private displayProgram: WglProgram
  private displayVao: WglVertexArray

  private size: vec2

  constructor(gl: WebGLRenderingContext, moleculeOptions: MoleculeOptions) {
    this.gl = gl
    this.updateLoop = new UpdateLoop(this.fixedStepUpdate.bind(this), this.frameUpdate.bind(this))
    this.molecules = new MoleculeCompute(gl, moleculeOptions)

    setGlDefaults(gl)

    this.displayProgram = new WglProgram(gl)

    this.displayProgram.attachAndLink(
      new WglShader(gl, gl.VERTEX_SHADER).loadAndCompile(moleculeDisplayVert),
      new WglShader(gl, gl.FRAGMENT_SHADER).loadAndCompile(moleculeDisplayFrag)
    )

    this.displayVao = new WglVertexArray(gl, 0, [
      {
        location: this.displayProgram.getAttribLocation(`moleculeIndex`),
        size: 2,
        type: gl.FLOAT,
        offset: 0
      }
    ])

    this.size = vec2.fromValues(1, 1)
  }

  public setMoleculeOptions(moleculeOptions: MoleculeOptions) {
    this.molecules.options = moleculeOptions
  }

  public run(): void {
    this.updateLoop.run()
  }

  public stop(): void {
    this.updateLoop.stop()
  }

  public resize(width: number, height: number) {
    this.size = vec2.fromValues(width, height)
    this.molecules.resetWorld(this.size)
  }

  public reset() {
    this.molecules.resetWorld(this.size, true)
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  private fixedStepUpdate(status: UpdateLoopStatus) {
    this.molecules.update(status)
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  private frameUpdate(status: UpdateLoopStatus) {
    const gl = this.gl
    gl.viewport(0, 0, this.size[0], this.size[1])
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
    this.molecules.indexVbo.bind(gl.ARRAY_BUFFER, (boundVbo) => {
      this.displayVao.enableAttribs(boundVbo)
      this.displayProgram.use((boundProgram) => {
        this.molecules.currMolecules.activateAndBind(gl.TEXTURE_2D, 0, (boundMoleculeTexture) => {
          boundProgram
            .setTextureUniform(`molecules`, boundMoleculeTexture, 0)
            .setFloatUniform(`textureSize`, this.molecules.textureSize)
            .setFloatUniform(`worldSize`, this.size)
            .setFloatUniform(`pointSize`, [this.molecules.options.pointRadius * 2])
          this.displayVao.drawArrays(boundProgram, boundVbo, gl.POINTS, 0, this.molecules.count)
        })
      })
    })
  }
}

function setGlDefaults(gl: WebGLRenderingContext) {
  gl.clearColor(0.0, 0.0, 0.0, 1.0)

  // Enable depth testing, so that objects are occluded based
  // on depth instead of drawing order.
  gl.enable(gl.DEPTH_TEST)

  // Move the polygons back a bit so lines are still drawn
  // even though they are coplanar with the polygons they
  // came from, which will be drawn before them.
  gl.enable(gl.POLYGON_OFFSET_FILL)
  gl.polygonOffset(-1, -1)

  // Enable back-face culling, meaning only the front side
  // of every face is rendered.
  gl.enable(gl.CULL_FACE)
  gl.cullFace(gl.BACK)

  // Specify that the front face is represented by vertices in
  // a counterclockwise order (this is the default).
  gl.frontFace(gl.CCW)
}
