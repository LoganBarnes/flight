class Prompt {
  public value: string
  public text: string
  public certification: string
  public content: string

  constructor(certification: string, displayText: string, response: string) {
    this.value = certification + displayText
    this.text = displayText
    this.certification = certification
    this.content = response
  }
}

const prompts: Prompt[] = [
  // SOLO
  new Prompt(
    `solo`,
    `Preflight categories`,
    [`PAVE`, `Pilot in command`, `Aircraft`, `enVironment`, `External pressures`].join(`\n`)
  ),
  new Prompt(
    `solo`,
    `Required docs for flying`,
    [`MICE`, `Medical Certificate`, `ID (Photo)`, `Certificate (Pilot)`, `Endorsements`].join(`\n`)
  ),
  new Prompt(
    `solo`,
    `Pilot physical & mental readiness`,
    [`IMSAFE`, `Illness`, `Medication`, `Stress`, `Alcohol`, `Fatigue`, `Emotion/Eating`].join(`\n`)
  ),
  new Prompt(
    `solo`,
    `Hazardous attitudes`,
    [`IMAIR`, `Invulnerability`, `Macho`, `Anti-authority`, `Impulsivity`, `Resignation`].join(`\n`)
  ),
  new Prompt(
    `solo`,
    `Aeromedical Illusions`,
    [
      `NICEFFLAGS`,
      `Night Myopia`,
      `Inversion Illusion`,
      `Coriolis Illusion`,
      `Elevator Illusion`,
      `False Horizon`,
      `Flicker Vertigo`,
      `Leans`,
      `Autokinesis`,
      `Graveyard Spiral`,
      `Somatogravic Illusion`
    ].join(`\n`)
  ),
  new Prompt(
    `solo`,
    `Aircraft documents`,
    [
      `ARROW`,
      `Airworthiness certificate`,
      `Registration (7 yrs)`,
      `Radio operator permit (international)`,
      `pilot Operators handbook`,
      `Weight & balance`
    ].join(`\n`)
  ),
  new Prompt(
    `solo`,
    `Preflight environment checks`,
    [
      `NWKRAFT`,
      `NOTAMs/TFRs`,
      `Weather`,
      `Known ATC delays`,
      `Runway lengths/information`,
      `Alternates`,
      `Fuel requirements`,
      `Takeoff & landing performance`
    ].join(`\n`)
  ),
  new Prompt(
    `solo`,
    `External Pressures`,
    [`GAPE`, `Get-there-itis`, `Attitudes`, `Passengers`, `Employers`].join(`\n`)
  ),
  new Prompt(
    `solo`,
    `FAA required VFR equipment (day)`,
    [
      `ATOMATOFLAMES`,
      `Altimeter`,
      `Tachometer`,
      `Oil pressure gauge`,
      `Manifold pressure gauge`,
      `Airspeed indicator`,
      `Temperature gauge`,
      `Oil temperature gauge`,
      `Fuel gauge`,
      `Landing gear position indicator`,
      `Anti-collision lights (planes)`,
      `Magnetic direction indicator`,
      `Emergency locator transmitter`,
      `Seat belts and flotation gear`
    ].join(`\n`)
  ),
  new Prompt(
    `solo`,
    `FAA required VFR equipment (night)`,
    [
      `FLAPS`,
      `Fuses`,
      `Landing light`,
      `Anti-collision lights`,
      `Position lights`,
      `Source of electricity`
    ].join(`\n`)
  ),
  new Prompt(
    `solo`,
    `Robinson required equipment (day)`,
    [
      `GOALH`,
      `Governor`,
      `Outside air temperature gage`,
      `Alternator`,
      `Low RPM warning system`,
      `Hydraulic control system`
    ].join(`\n`)
  ),
  new Prompt(
    `solo`,
    `Robinson required equipment (night)`,
    [
      `NAILRCG`,
      `Navigation lights`,
      `Anti-collision lights`,
      `Instrument lights`,
      `Landing lights`,
      `Reference to ground via`,
      `Celestial (adequate illumination) or`,
      `Ground lights`
    ].join(`\n`)
  ),
  new Prompt(
    `solo`,
    `Aeronautical Decision Making models`,
    [
      `DECIDE`,
      `Detect the problem`,
      `Estimate the need to react`,
      `Choose a desired outcome`,
      `Identify solutions`,
      `Do the necessary actions`,
      `Evaluate the effect of the actions`,
      ``,
      `PPP`,
      `Perceive`,
      `Process`,
      `Perform`
    ].join(`\n`)
  ),
  new Prompt(
    `solo`,
    `Lost procedures`,
    [`CCCCC`, `Confess`, `Climb`, `Conserve`, `Communicate`, `Comply`].join(`\n`)
  ),

  // PRIVATE
  new Prompt(
    `private`,
    `Private Eligibility Requirements`,
    [
      `TEAMCEE`,
      `Tests (written and knowledge)`,
      `English`,
      `Age (17)`,
      `Medical Certificate 3rd class`,
      `Certificate (student)`,
      `Endorsements`,
      `Experience`
    ].join(`\n`)
  ),
  new Prompt(
    `private`,
    `Private Pilot Limitations`,
    [
      `SPINSTT`,
      `Search & Locate`,
      `Pro rata share`,
      `Incidental to business`,
      `Non-profit`,
      `Sales`,
      `Testing`,
      `Towing`
    ].join(`\n`)
  ),
  new Prompt(
    `private`,
    `Required Inspections`,
    [
      `AVIATESLAST`,
      `Annual`,
      `VOR (30 days for IFR)`,
      `100 hour`,
      `Altimeter (24 months)`,
      `Transponder (24 months)`,
      `ELT (12 months)`,
      `Static/Pitot (24 months)`,
      `Life limited parts`,
      `Airworthiness directives`,
      `Service Bulletins`,
      `Time limited parts`
    ].join(`\n`)
  ),
  new Prompt(
    `private`,
    `Preflight Passenger Briefing`,
    [
      `SAFETY`,
      `Seat belts`,
      `Air vents`,
      `Fire extinguisher`,
      `Exit/Emergencies`,
      `Traffic/Talking`,
      `Your questions`
    ].join(`\n`)
  ),
  new Prompt(
    `private`,
    `Off Airport Landings`,
    [
      `AWOTFEEL`,
      `Altitude`,
      `Weather/Wind`,
      `Obstacles`,
      `Terrain`,
      `Forced landing`,
      `Entry`,
      `Exit`,
      `Landing area`
    ].join(`\n`)
  ),
  new Prompt(
    `private`,
    `NTSB Immediate Notification`,
    [
      `PTFRACTIONS`,
      `Property damage`,
      `TCAS/ACAS resolution on IFR flight plan`,
      `Fire in flight`,
      `Rotor damage`,
      `Aircraft accident`,
      `Collision in flight`,
      `Turbine failure`,
      `Inability of aircrew`,
      `Overdue aircraft`,
      `No control`,
      `Screen failure`
    ].join(`\n`)
  )
]

function getPrompts(certification: string): Prompt[] {
  return prompts.filter((i: Prompt) => certification === `all` || i.certification === certification)
}

export function getQuestions(certification: string): { text: string; value: string }[] {
  return getPrompts(certification)
}

function getAnswers(certification: string): Map<string, string> {
  return new Map(getPrompts(certification).map((i: Prompt) => [i.value, i.content]))
}

export function getAnswer(certification: string, question: string): string {
  return getAnswers(certification).get(question) || ``
}
