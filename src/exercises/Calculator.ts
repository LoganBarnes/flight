export interface CalculatorInput {
  over2200Togw: boolean
  anyDoorsOff: boolean
  altitudeFt: number
  oatC: number
}

export interface CalculatorOutput {
  maxContinuousMap: number
  maxTakeoffMap: number
  maxMapOffAirport: number
  maxAirspeed: number
  maxAirspeedAutorotation: number
}

interface Limits {
  minIndex: number
  maxIndex: number
  alpha: number
}

function linearInterpolation(q0: number, q1: number, a: number) {
  return (1.0 - a) * q0 + a * q1
}

function bilinearInterpolation(
  q00: number,
  q01: number,
  q10: number,
  q11: number,
  a0: number,
  a1: number
) {
  const q0 = linearInterpolation(q00, q01, a0)
  const q1 = linearInterpolation(q10, q11, a0)
  const q = linearInterpolation(q0, q1, a1)
  return q
}

export class Calculator {
  private mapTable: number[][]
  private airspeedTable: number[][]

  constructor() {
    this.mapTable = [
      [21.5, 21.8, 22.1, 22.4, 22.6, 22.9, 23.1, 23.3],
      [20.9, 21.2, 21.5, 21.8, 22.1, 22.3, 22.5, 22.8],
      [20.4, 20.7, 21.0, 21.3, 21.5, 21.8, 22.0, 22.2],
      [19.9, 20.2, 20.5, 20.8, 21.0, 21.3, 21.5, 21.7],
      [19.5, 19.8, 20.1, 20.3, 20.6, 20.8, 21.0, 21.3],
      [19.1, 19.4, 19.6, 19.9, 0.0, 0.0, 0.0, 0.0],
      [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    ]

    this.airspeedTable = [
      [130, 130, 130, 130, 130, 130, 130, 130],
      [130, 130, 130, 130, 130, 130, 127, 123],
      [130, 130, 130, 130, 126, 122, 118, 114],
      [130, 130, 126, 122, 117, 113, 108, 103],
      [126, 122, 117, 112, 107, 101, 96, 91],
      [117, 112, 106, 101, 95, 90, 85, 0],
      [107, 101, 95, 89, 0, 0, 0, 0],
      [95, 89, 0, 0, 0, 0, 0, 0]
    ]
  }

  public calculate(input: CalculatorInput): CalculatorOutput {
    const maxContinuousMap = this.getContinuousMap(input)
    let maxTakeoffMap = maxContinuousMap

    if (maxContinuousMap !== 0) {
      maxTakeoffMap += 2.8
    }

    const maxMapOffAirport = maxTakeoffMap * 0.85

    const topAirspeed = this.getMaxAirspeed(input)
    let maxAirspeedAutorotation = topAirspeed - 30.0
    let maxAirspeed = input.over2200Togw ? topAirspeed - 10.0 : topAirspeed

    if (input.anyDoorsOff) {
      maxAirspeedAutorotation = Math.min(maxAirspeedAutorotation, 100.0)
      maxAirspeed = Math.min(maxAirspeed, 100.0)
    }

    return {
      maxContinuousMap,
      maxTakeoffMap,
      maxMapOffAirport,
      maxAirspeed,
      maxAirspeedAutorotation
    }
  }

  get mapTableValues(): number[][] {
    return this.mapTable
  }

  get airspeedTableValues(): number[][] {
    return this.airspeedTable
  }

  private getAltIndices(altFt: number): Limits {
    const quotient = altFt / 2000.0
    const lowerLimitAlt = Math.floor(quotient)
    const alpha = quotient - lowerLimitAlt
    return {
      minIndex: lowerLimitAlt,
      maxIndex: lowerLimitAlt + 1,
      alpha
    }
  }

  private getIndices(quotient: number): Limits {
    const lowerLimitTemp = Math.floor(quotient)
    const minIndex = Math.max(lowerLimitTemp, 0)
    const maxIndex = Math.min(lowerLimitTemp + 1, 7)
    const alpha = quotient - lowerLimitTemp
    return {
      minIndex,
      maxIndex,
      alpha
    }
  }

  private getMap(altLimits: Limits, tempLimits: Limits): number {
    const map00 = this.mapTable[altLimits.minIndex][tempLimits.minIndex]
    const map01 = this.mapTable[altLimits.minIndex][tempLimits.maxIndex]
    const map10 = this.mapTable[altLimits.maxIndex][tempLimits.minIndex]
    const map11 = this.mapTable[altLimits.maxIndex][tempLimits.maxIndex]

    if (map00 === 0.0 || map01 === 0.0 || map10 === 0.0 || map11 === 0.0) {
      return 0.0
    }

    return bilinearInterpolation(map00, map01, map10, map11, tempLimits.alpha, altLimits.alpha)
  }

  private getContinuousMap(input: CalculatorInput): number {
    const altLimits = this.getIndices(input.altitudeFt / 2000.0)
    const tempLimits = this.getIndices((input.oatC + 30.0) / 10.0)
    return this.getMap(altLimits, tempLimits)
  }

  private getAirspeed(altLimits: Limits, tempLimits: Limits): number {
    const airspeed00 = this.airspeedTable[altLimits.minIndex][tempLimits.minIndex]
    const airspeed01 = this.airspeedTable[altLimits.minIndex][tempLimits.maxIndex]
    const airspeed10 = this.airspeedTable[altLimits.maxIndex][tempLimits.minIndex]
    const airspeed11 = this.airspeedTable[altLimits.maxIndex][tempLimits.maxIndex]

    if (airspeed00 === 0.0 || airspeed01 === 0.0 || airspeed10 === 0.0 || airspeed11 === 0.0) {
      return 0.0
    }

    return bilinearInterpolation(
      airspeed00,
      airspeed01,
      airspeed10,
      airspeed11,
      tempLimits.alpha,
      altLimits.alpha
    )
  }

  private getMaxAirspeed(input: CalculatorInput): number {
    const altLimits = this.getAltIndices(input.altitudeFt)
    const tempLimits = this.getIndices((input.oatC + 30.0) / 10.0)
    return this.getAirspeed(altLimits, tempLimits)
  }
}
