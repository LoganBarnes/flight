export interface WglBufferData {
  buffer: WebGLBuffer
  numElements: number
  sizeInBytes: number
}

export class WglBuffer {
  private gl: WebGLRenderingContext
  private _data: WglBufferData

  constructor(gl: WebGLRenderingContext) {
    this.gl = gl

    const buffer = gl.createBuffer()
    if (buffer === null) {
      throw new Error(`Failed to create WebGL buffer`)
    }

    this._data = {
      buffer: buffer,
      numElements: 0,
      sizeInBytes: 0
    }
  }

  get data(): WglBufferData {
    return this._data
  }

  public bind(
    type: GLenum,
    usage: (boundBuffer: InstanceType<typeof WglBuffer.Bound>) => void
  ): WglBuffer {
    this.gl.bindBuffer(type, this.data.buffer)
    usage(new WglBuffer.Bound(this, type))
    this.gl.bindBuffer(type, null)
    return this
  }

  static Bound = class {
    constructor(
      public self: WglBuffer,
      public readonly type: GLenum
    ) {
      this.self = self
      this.type = type
    }

    get buffer(): WebGLBuffer {
      return this.self._data.buffer
    }

    public bufferData(
      bufferData: ArrayBuffer | null,
      usage: GLenum = this.self.gl.STATIC_DRAW
    ): InstanceType<typeof WglBuffer.Bound> {
      const gl = this.self.gl
      gl.bufferData(this.type, bufferData, usage)
      return this
    }

    public bufferSubData(
      offset: number,
      bufferData: ArrayBuffer
    ): InstanceType<typeof WglBuffer.Bound> {
      const gl = this.self.gl
      gl.bufferSubData(this.type, offset, bufferData)
      return this
    }
  }
}
