import { WglBuffer, WglProgram } from '@/graphics'

export interface WglVaoElement {
  location: number
  size: number
  type: GLenum
  offset: number
}

export class WglVertexArray {
  private gl: WebGLRenderingContext
  private totalStride: number
  private elements: WglVaoElement[]

  constructor(gl: WebGLRenderingContext, totalStride: number, elements: WglVaoElement[]) {
    this.gl = gl
    this.totalStride = totalStride
    this.elements = elements
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public enableAttribs(_vbo: InstanceType<typeof WglBuffer.Bound>): WglVertexArray {
    const gl = this.gl

    for (const element of this.elements) {
      gl.enableVertexAttribArray(element.location)
      gl.vertexAttribPointer(
        element.location,
        element.size,
        element.type,
        false,
        this.totalStride,
        element.offset
      )
    }
    return this
  }

  public drawArrays(
    _program: InstanceType<typeof WglProgram.Bound>,
    _vbo: InstanceType<typeof WglBuffer.Bound>,
    mode: GLenum,
    first: number,
    count: number
  ): WglVertexArray {
    const gl = this.gl
    gl.drawArrays(mode, first, count)
    return this
  }

  public drawElements(
    _program: InstanceType<typeof WglProgram.Bound>,
    _vbo: InstanceType<typeof WglBuffer.Bound>,
    _ibo: InstanceType<typeof WglBuffer.Bound>,
    mode: GLenum,
    count: number,
    type: GLenum,
    offset: number
  ): WglVertexArray {
    const gl = this.gl
    gl.drawElements(mode, count, type, offset)
    return this
  }
}
