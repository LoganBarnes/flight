export interface WglShaderData {
  shader: WebGLShader
  type: GLenum
}

export class WglShader {
  private gl: WebGLRenderingContext
  private _data: WglShaderData

  constructor(gl: WebGLRenderingContext, type: GLenum) {
    this.gl = gl

    const shader: WebGLProgram | null = gl.createShader(type)
    if (shader === null) {
      throw new Error('Failed to create WebGL shader')
    }

    this._data = {
      shader: shader,
      type: type
    }
  }

  get data(): WglShaderData {
    return this._data
  }

  public loadAndCompile(text: string): WglShader {
    const gl = this.gl

    gl.shaderSource(this.data.shader, text)
    gl.compileShader(this.data.shader)

    if (!gl.getShaderParameter(this.data.shader, gl.COMPILE_STATUS)) {
      throw new Error(`Failed to compile shader: ${gl.getShaderInfoLog(this.data.shader)}`)
    }

    return this
  }
}
