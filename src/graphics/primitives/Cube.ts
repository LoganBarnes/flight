// Unit cube centered on origin
export class Cube {
  private vertData: Float32Array
  private normData: Float32Array
  private uvData: Float32Array

  private idxData: Uint16Array

  constructor() {
    const r = 0.5

    // prettier-ignore
    const vertexData: number[] = [
      // pos-z face
      -r, -r, +r,
      +r, -r, +r,
      -r, +r, +r,
      +r, +r, +r,
      // neg-z face
      +r, -r, -r,
      -r, -r, -r,
      +r, +r, -r,
      -r, +r, -r,
      // pos-x face
      +r, -r, +r,
      +r, -r, -r,
      +r, +r, +r,
      +r, +r, -r,
      // neg-x face
      -r, -r, -r,
      -r, -r, +r,
      -r, +r, -r,
      -r, +r, +r,
      // pos-y face
      -r, +r, +r,
      +r, +r, +r,
      -r, +r, -r,
      +r, +r, -r,
      // neg-y face
      -r, -r, -r,
      +r, -r, -r,
      -r, -r, +r,
      +r, -r, +r
    ]

    // prettier-ignore
    const normalData: number[] = [
      // pos-z face
      0, 0, 1,
      0, 0, 1,
      0, 0, 1,
      0, 0, 1,
      // neg-z face
      0, 0, -1,
      0, 0, -1,
      0, 0, -1,
      0, 0, -1,
      // pos-x face
      1, 0, 0,
      1, 0, 0,
      1, 0, 0,
      1, 0, 0,
      // neg-x face
      -1, 0, 0,
      -1, 0, 0,
      -1, 0, 0,
      -1, 0, 0,
      // pos-y face
      0, 1, 0,
      0, 1, 0,
      0, 1, 0,
      0, 1, 0,
      // neg-y face
      0, -1, 0,
      0, -1, 0,
      0, -1, 0,
      0, -1, 0
    ]

    // prettier-ignore
    const textureCoordinateData: number[] = [
      // pos-z face
      0, 0,
      1, 0,
      0, 1,
      1, 1,
      // neg-z face
      0, 0,
      1, 0,
      0, 1,
      1, 1,
      // pos-x face
      0, 0,
      1, 0,
      0, 1,
      1, 1,
      // neg-x face
      0, 0,
      1, 0,
      0, 1,
      1, 1,
      // pos-y face
      0, 0,
      1, 0,
      0, 1,
      1, 1,
      // neg-y face
      0, 0,
      1, 0,
      0, 1,
      1, 1
    ]

    // prettier-ignore
    const indexData: number[] = [
      // pos-z face
      0, 1, 2,
      2, 1, 3,
      // neg-z face
      4, 5, 6,
      6, 5, 7,
      // pos-x face
      8, 9, 10,
      10, 9, 11,
      // neg-x face
      12, 13, 14,
      14, 13, 15,
      // pos-y face
      16, 17, 18,
      18, 17, 19,
      // neg-y face
      20, 21, 22,
      22, 21, 23
    ]

    this.vertData = new Float32Array(vertexData)
    this.normData = new Float32Array(normalData)
    this.uvData = new Float32Array(textureCoordinateData)

    this.idxData = new Uint16Array(indexData)
  }

  get vertexData(): Float32Array {
    return this.vertData
  }

  get normalData(): Float32Array {
    return this.normData
  }

  get textureCoordinateData(): Float32Array {
    return this.uvData
  }

  get indexData(): Uint16Array {
    return this.idxData
  }

  public getPrimitiveType(gl: WebGLRenderingContext): GLenum {
    return gl.TRIANGLES
  }

  public getVertexDataSizeInBytes(): number {
    return this.vertData.length * 4
  }

  public getNormalDataSizeInBytes(): number {
    return this.normData.length * 4
  }

  public getUvDataSizeInBytes(): number {
    return this.uvData.length * 4
  }

  public buildArrayWithAllVertexData(): Float32Array {
    const verts: Float32Array = this.vertexData
    const norms: Float32Array = this.normalData
    const uvs: Float32Array = this.uvData

    const data: Float32Array = new Float32Array(verts.length + norms.length + uvs.length)
    data.set(verts)
    data.set(norms, verts.length)
    data.set(uvs, verts.length + norms.length)

    return data
  }
}
