#version 100
precision highp float;
precision highp int;

const ivec2 PARTICLE_TEX_SIZE = ivec2(replace_with_particle_tex_size);

uniform sampler2D prevMolecules;
uniform vec2      textureSize;

uniform sampler2D spatialMolecules;
uniform vec2      worldSize;

uniform float timeStepSecs;

uniform float groundTemperature;
uniform float gravity;
uniform float pointRadius;

float rand(vec2 co) {
  return fract(sin(dot(co, vec2(12.9898, 78.233))) * 43758.5453);
}

void main() {
  float pointDiameter   = pointRadius * 2.0;
  float pointDiameterSq = pointDiameter * pointDiameter;

  vec2 index = gl_FragCoord.xy;

  vec4 posAndVel = texture2D(prevMolecules, index / textureSize).xyzw;
  vec2 pos       = posAndVel.xy;
  vec2 vel       = posAndVel.zw;

  {
    vec2 cumulativePos = pos;
    vec2 cumulativeVel = vel;

    for (int yi = 0; yi < PARTICLE_TEX_SIZE.y; ++yi) {
      for (int xi = 0; xi < PARTICLE_TEX_SIZE.x; ++xi) {
        if (xi == int(floor(gl_FragCoord.x)) && yi == int(floor(gl_FragCoord.y))) {
          continue;
        }
        vec4 otherPosAndVel = texture2D(prevMolecules, (vec2(xi, yi) + vec2(0.5)) / textureSize).xyzw;
        vec2 otherPos       = otherPosAndVel.xy;
        vec2 otherVel       = otherPosAndVel.zw;

        vec2 tmpPos = pos;
        if (otherPos == tmpPos) {
          float offsetX = rand(gl_FragCoord.xy + cumulativePos);
          float offsetY = rand(gl_FragCoord.xy + cumulativePos + offsetX);
          tmpPos += normalize(vec2(offsetX, offsetY)) * 0.001;
        }

        vec2  delta  = tmpPos - otherPos;
        float distSq = dot(delta, delta);
        if (distSq < pointDiameterSq) {
          float dist     = sqrt(distSq);
          vec2  deltaDir = delta / dist;

          cumulativeVel -= delta * (dot(cumulativeVel - otherVel, delta) / distSq);
          cumulativePos += deltaDir * (pointDiameter - dist) * 0.5;
        }
      }
    }

    vel = cumulativeVel + vec2(0.0, -gravity) * timeStepSecs;
    pos = cumulativePos + vel * timeStepSecs;
  }

  if (pos.y < pointRadius) {
    pos.y = pointRadius;
    vel.y = groundTemperature;
  }
  // Particles are allowed extend past the top of the world.
  
  if (pos.x < pointRadius) {
    pos.x = pointRadius;
    vel.x = -vel.x;
  } else if (pos.x > worldSize.x - pointRadius) {
    pos.x = worldSize.x - pointRadius;
    vel.x = -vel.x;
  }

  gl_FragColor = vec4(pos, vel);
}
