#version 100
precision highp float;
precision highp int;

varying vec3 color;

void main() {
  vec2  coords = gl_PointCoord * 2.0 - 1.0;
  float dist   = dot(coords, coords);
  if (dist > 1.0)
  {
    discard;
  }
  // Darken the edges
  float intensity = 1.0 - dist * dist * dist * dist;
  gl_FragColor    = vec4(color * intensity, 1.0);
}
