#version 100
precision highp float;
precision highp int;

attribute vec2 moleculeIndex;

uniform sampler2D molecules;
uniform vec2 textureSize;
uniform vec2 worldSize;
uniform float pointSize;

varying vec3 color;

vec3 colorFromSpeed(float speed) {
  float r = clamp(speed - 1.5, 0.0, 1.0);
  float g = clamp(1.5 - abs(speed - 1.5), 0.0, 1.0);
  float b = clamp(1.5 - speed, 0.0, 1.0);
  return vec3(r, g, b);
}

void main() {
  vec4 posAndVel = texture2D(molecules, moleculeIndex / textureSize).xyzw;
  vec2 moleculeWorldPosition = posAndVel.xy;
  vec2 moleculeVelocity = posAndVel.zw;

  float speed = length(moleculeVelocity) / 25.0; // [0, 4]
  color = colorFromSpeed(speed);

  vec2 moleculeNormalizedPosition = moleculeWorldPosition / worldSize;
  vec2 moleculeClipPosition = moleculeNormalizedPosition * 2.0 - 1.0;

  gl_Position = vec4(moleculeClipPosition, 0.5, 1.0);
  gl_PointSize = pointSize;
}
