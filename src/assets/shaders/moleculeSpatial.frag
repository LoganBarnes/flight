#version 100
precision highp float;
precision highp int;

varying vec2 index;
varying vec2 velocity;

void main() {
  gl_FragColor = vec4(index, velocity);
}
