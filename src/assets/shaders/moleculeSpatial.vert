#version 100
precision highp float;
precision highp int;

attribute vec2 moleculeIndex;

uniform sampler2D molecules;
uniform vec2 textureSize;
uniform vec2 worldSize;

varying vec2 index;
varying vec2 velocity;

void main() {
  vec4 posAndVel = texture2D(molecules, moleculeIndex / textureSize).xyzw;
  vec2 moleculeWorldPosition = posAndVel.xy;
  vec2 moleculeVelocity = posAndVel.zw;

  index = moleculeIndex + vec2(1.0);
  velocity = moleculeVelocity;

  vec2 moleculeNormalizedPosition = moleculeWorldPosition / worldSize;
  vec2 moleculeClipPosition = moleculeNormalizedPosition * 2.0 - 1.0;

  gl_Position = vec4(moleculeClipPosition, 0.5, 1.0);
  gl_PointSize = 1.0;
}
