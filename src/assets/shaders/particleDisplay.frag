#version 100
precision highp float;
precision highp int;

varying vec2 uv;

uniform sampler2D flowMap;

void main() {
  vec3 flow = texture2D(flowMap, uv).xyz;
  gl_FragColor = vec4(flow, 1.0);
}
