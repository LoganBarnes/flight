#version 100
precision highp float;
precision highp int;

attribute vec2 cornerClipPos;

void main() {
  gl_Position = vec4(cornerClipPos, 0.5, 1.0);
}
