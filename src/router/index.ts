import { createRouter, createWebHistory } from 'vue-router'
import AboutView from '../views/AboutView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'about',
      component: AboutView
    },
    {
      path: '/test',
      name: 'test',
      // route level code-splitting
      // this generates a separate chunk (Test.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/TestView.vue')
    },
    {
      path: '/calculator',
      name: 'calculator',
      component: () => import('../views/CalculatorView.vue')
    },
    {
      path: '/poh',
      name: 'poh',
      component: () => import('../views/PohView.vue')
    },
    {
      path: '/acronyms',
      name: 'acronyms',
      component: () => import('../views/AcronymsView.vue')
    },
    {
      path: '/definitions',
      name: 'definitions',
      component: () => import('../views/DefinitionsView.vue')
    },
    {
      path: '/air',
      name: 'air',
      component: () => import('../views/AirView.vue')
    }
  ]
})

export default router
